package com.example.affirmations.model

data class Affirmation (@StringRes val stringResourceId: Int,@DrawableRes val imageResourceId: Int)